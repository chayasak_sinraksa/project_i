using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayNormalGame()
    {
        SceneManager.LoadScene(1);
    }
    
    public void PlayHardGame()
    {
        SceneManager.LoadScene(2);
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Debug.Log("GoodBye!");
        Application.Quit();
    }
}
